<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class fichaTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function create_ficha_test()
    {
        $ficha = Factory(\App\Ficha::class)->create();

        $this->assertEquals(1, $ficha->count());
    }

    /** @test */
    public function fichas_is_related_to_fondos()
    {
        $ficha = Factory(\App\Ficha::class)->create();
        $fondos = \App\Fondo::find($ficha->fondo_id);

        $this->assertEquals($ficha->fondos->nombre, $fondos->nombre);
    }

    /** @test */
    public function fichas_is_related_edificios()
    {
        $ficha = Factory(\App\Ficha::class)->create();
        $edificio = \App\Edificio::find($ficha->edificio_id);

        $this->assertEquals($ficha->edificios->id, $edificio->id);
    }

    /** @test */
    public function fichas_is_related_tipoapartamento()
    {
        $ficha = Factory(\App\Ficha::class)->create();
        $tipo = \App\Tipoapartamento::find($ficha->tipoapartamento_id);

        $this->assertEquals($ficha->tipoapartamento->tipo, $tipo->tipo);
    }

}
