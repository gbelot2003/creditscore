<?php

use App\Departamento;
use App\Edificio;
use App\Mancomunado;
use App\Municipio;
use App\Solicitud;
use App\Tipoapartamento;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class solicitudsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function create_solicitud_amd_mancomunado()
    {
        $solicitud = factory(Solicitud::class)->create();
        $mancomunado = factory(Mancomunado::class)->create(
            ['solicitud_id' => $solicitud->id]
        );

        $this->assertEquals(1, $solicitud->count());
        $this->assertEquals(1, $mancomunado->count());
    }

    /** @test */
    public function create_solicitud_have_mancomunado_properties()
    {
        $solicitud = factory(Solicitud::class)->create();
        $mancomunado = factory(Mancomunado::class)->create(
            [
                'solicitud_id' => $solicitud->id,
                'nombre' => 'JonDeep'
            ]
        );

        $this->assertEquals($mancomunado->nombre, $solicitud->mancomunado->nombre);

    }

    /** @test */
    public function create_mancomunado_have_solicitud_properties()
    {
        $solicitud = factory(Solicitud::class)->create(
            [
                'nombre' => 'JonhanDeep'
            ]
        );
        $mancomunado = factory(Mancomunado::class)->create(
            [
                'solicitud_id' => $solicitud->id,
                'nombre' => 'JonDeep'
            ]
        );

        $this->assertEquals($mancomunado->solicitudes->nombre, $solicitud->nombre);
    }


    /** @test */
    public function solicitud_trying_municipios_relationship()
    {

        $municipio = Municipio::find(2);

        $solicitud = factory(Solicitud::class)->create(
            [
                'departamento_id' => 1,
                'municipio_id' => 2
            ]
        );

        $mancomunado = factory(Mancomunado::class)->create(
            [
                'departamento_id' => 1,
                'municipio_id' => 2
            ]
        );

        $this->assertEquals($solicitud->municipio->municipio, $municipio->municipio);
        $this->assertEquals($mancomunado->municipio->municipio, $municipio->municipio);

    }

    /** @test */
    public function solicitud_trying_departamentos_reach()
    {
        $depto = Departamento::find(1);

        $solicitud = factory(Solicitud::class)->create(
            [
                'departamento_id' => 1,
                'municipio_id' => 2
            ]
        );

        $mancomunado = factory(Mancomunado::class)->create(
            [
                'departamento_id' => 1,
                'municipio_id' => 2
            ]
        );

        $this->assertEquals($solicitud->municipio->departamento->departamento, $depto->departamento);
        $this->assertEquals($mancomunado->municipio->departamento->departamento, $depto->departamento);
    }

    /** @test */
    public function solicitud_try_edificios_rel()
    {
        $edificio = Edificio::find(1);

        $solicitud = factory(Solicitud::class)->create(
            [
                'edificio_id' => 1,
            ]
        );

        $this->assertEquals($edificio->area_comun, $solicitud->edificio->area_comun);
    }


    /** @test */
    public function solicitud_try_tipoapartamentos()
    {
        $tipo = Tipoapartamento::find(2);
        $solicitud = factory(Solicitud::class)->create(
            [
                'tipoapartamento_id' => 2,
            ]
        );

        $this->assertEquals($tipo->tipo, $solicitud->tipoapartamentos->tipo);

    }

    /** @test */
    public function solicitud_calculate_nomenclatura()
    {
        $solicitud = factory(Solicitud::class)->create(
            [
                'nomenclatura' => '2A-120'
            ]
        );

        $this->assertEquals($solicitud->nomenclatura, '2A-120');
    }

}





































