<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{

    protected $table = "solicituds";

    protected $tamanioApartamento = [43, 45];

    protected $tamanioCasa = [55];

    protected $cluster = [1, 2, 7];

    protected $bloque = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                        'I', 'E-ad', 'C-ad', 'I-ad', 'H-ad'
    ];

    protected $edificio = [1, 2, 3, 4, 5, 6, 7, 8];

    protected $nivel = [1, 2, 3];

    protected $capacidadApartamentosEdificio = [12, 24];

    protected $apartamento = [10, 11, 12, 13, 14, 15, 16, 17,
                              20, 21, 22, 23, 24, 25, 26, 27,
                              30, 31, 32, 33, 34, 35, 36, 37
    ];

    /**
     * @var array
     */
    protected $apartamentsNivel = [4, 8];

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'apellido', 'estado_civil', 'profesion', 'telefono', 'celular','domicilio',
        'municipio_id', 'departamento_id', 'email', 'tipoapartamento_id', 'tamanio_apartamento', 'tamanio_casa',
        'cluster', 'bloque', 'edificio_id', 'nivel_piso', 'apartamento', 'lote_casa', 'nomenclatura', 'nomenclatura_casa',
        'tamanio_lote_metros', 'tamanio_lote_varas', 'capacidad_apartamentos_edificio', 'tamanio_edificio',
        'apartaments_nivel', 'area_comun', 'precio_venta', 'prima', 'monto_financiar'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mancomunado()
    {
        return $this->hasOne(Mancomunado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoapartamentos()
    {
        return $this->belongsTo(Tipoapartamento::class, 'tipoapartamento_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function edificio()
    {
        return $this->belongsTo(Edificio::class);
    }


}











































