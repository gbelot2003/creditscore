<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;


    public function departamento()
    {
        return $this->belongsTo(Departamento::class);
    }

}
