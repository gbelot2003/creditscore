<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipoapartamento extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function solicitud()
    {
        return $this->hasMany(Solicitud::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mancomunado()
    {
        return $this->hasMany(Mancomunado::class);
    }
}
