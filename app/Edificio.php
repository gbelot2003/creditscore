<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edificio extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    public function solicitud()
    {
        return $this->hasMany(Solicitud::class);
    }
}
