<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mancomunado extends Model
{
    protected $fillable = ['nombre', 'solicitud_id', 'apellido', 'estado_civil', 'profesion', 'telefono', 'celular',
        'domicilio', 'municipio_id', 'departamento_id', 'email'];


    public function solicitudes()
    {
        return $this->belongsTo(Solicitud::class, 'solicitud_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }
}
