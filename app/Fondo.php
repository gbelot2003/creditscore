<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fondo extends Model
{
    public $timestamps = false;

    public function fichas()
    {
        return $this->hasOne(Ficha::class);
    }
}
