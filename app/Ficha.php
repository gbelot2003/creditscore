<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    protected $fillable = ['nombre', 'identidad', 'email', 'telefono', 'movil', 'con_nombre', 'con_identidad',
    'total_ingresos', 'fondo_id', 'tasa_apox', 'tipoapartamento_id', 'tamanio_apartamento', 'tamanio_casa',
    'cluster', 'bloque', 'edificio_id', 'nivel_piso', 'apartamento', 'lote_casa', 'tamanio_lote_metros', 'tamanio_lote_varas'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fondos()
    {
        return $this->belongsTo(Fondo::class, 'fondo_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function edificios()
    {
        return $this->belongsTo(Edificio::class, 'edificio_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoapartamento()
    {
        return $this->belongsTo(Tipoapartamento::class, 'tipoapartamento_id', 'id');
    }
}
