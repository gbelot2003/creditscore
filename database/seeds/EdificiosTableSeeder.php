<?php

use Illuminate\Database\Seeder;

class EdificiosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('edificios')->delete();
        
        \DB::table('edificios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'area_construccion_total' => 1027.1700000000001,
                'area_comun' => 33.920000000000002,
            ),
            1 => 
            array (
                'id' => 2,
                'area_construccion_total' => 1105.98,
                'area_comun' => 80.239999999999995,
            ),
            2 => 
            array (
                'id' => 3,
                'area_construccion_total' => 1105.98,
                'area_comun' => 80.239999999999995,
            ),
            3 => 
            array (
                'id' => 4,
                'area_construccion_total' => 1027.1700000000001,
                'area_comun' => 33.920000000000002,
            ),
            4 => 
            array (
                'id' => 5,
                'area_construccion_total' => 513.58000000000004,
                'area_comun' => 16.960000000000001,
            ),
            5 => 
            array (
                'id' => 6,
                'area_construccion_total' => 552.99000000000001,
                'area_comun' => 40.119999999999997,
            ),
            6 => 
            array (
                'id' => 7,
                'area_construccion_total' => 552.99000000000001,
                'area_comun' => 40.119999999999997,
            ),
            7 => 
            array (
                'id' => 8,
                'area_construccion_total' => 513.58000000000004,
                'area_comun' => 16.960000000000001,
            ),
        ));
        
        
    }
}
