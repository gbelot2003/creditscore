<?php

use Illuminate\Database\Seeder;

class FondosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fondos')->delete();
        
        \DB::table('fondos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Bancarios',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'BANHPROVI',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'RAP',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'IPM',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'INJUPEMP',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'IHSS',
            ),
        ));
        
        
    }
}
