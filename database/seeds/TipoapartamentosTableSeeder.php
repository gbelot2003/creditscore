<?php

use Illuminate\Database\Seeder;

class TipoapartamentosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipoapartamentos')->delete();
        
        \DB::table('tipoapartamentos')->insert(array (
            0 => 
            array (
                'id' => 1,
            'tipo' => 'VPI (Condominio Interior)',
            ),
            1 => 
            array (
                'id' => 2,
            'tipo' => 'VPE (Condominio Externo)',
            ),
            2 => 
            array (
                'id' => 3,
            'tipo' => 'Modelo Esperanza 1 (1 dormitorio)',
            ),
            3 => 
            array (
                'id' => 4,
            'tipo' => 'Modelo Esperanza 2 (2 dormitorios)',
            ),
            4 => 
            array (
                'id' => 5,
            'tipo' => 'Modelo Esperanza 3 (3 dormitorios)',
            ),
            5 => 
            array (
                'id' => 6,
                'tipo' => 'LOTE',
            ),
        ));
        
        
    }
}
