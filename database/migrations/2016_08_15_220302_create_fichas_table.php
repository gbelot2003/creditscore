<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('fondos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre');
            });

            Schema::create('fichas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('identidad');
            $table->string('email');
            $table->string('telefono');
            $table->string('celular');
            $table->string('con_nombre');
            $table->string('con_identidad');

            $table->boolean('bono');
            $table->double('total_ingresos');
            $table->integer('fondo_id')->unsigned()->index();
            $table->double('tasa_apox')->nullable();

            $table->integer('tipoapartamento_id')->unsigned()->index();
            $table->integer('tamanio_apartamento');
            $table->integer('tamanio_casa');
            $table->integer('cluster');
            $table->string('bloque');
            $table->integer('edificio_id')->unsigned()->index();
            $table->integer('nivel_piso');
            $table->integer('apartamento');
            $table->integer('lote_casa');
            $table->double('tamanio_lote_metros');
            $table->double('tamanio_lote_varas');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fichas');
        Schema::drop('fondos');
    }
}
