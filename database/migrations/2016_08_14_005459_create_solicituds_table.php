<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tipoapartamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
        });

        Schema::create('mancomunados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solicitud_id')->unsigned()->index();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('estado_civil');
            $table->string('profesion');
            $table->integer('identidad')->unsigned();
            $table->integer('telefono')->nullable();
            $table->integer('celular')->nullable();
            $table->text('domicilio');
            $table->integer('municipio_id')->unsigned()->index();
            $table->integer('departamento_id')->unsigned()->index();
            $table->string('email')->nullable();
            $table->timestamps();
        });

        Schema::create('edificios', function (Blueprint $table) {
            $table->increments('id');
            $table->double('area_construccion_total');
            $table->double('area_comun');
        });

        Schema::create('solicituds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('estado_civil');
            $table->string('profesion');
            $table->integer('identidad')->unsigned();
            $table->integer('telefono')->unsigned();
            $table->integer('celular')->unsigned();
            $table->text('domicilio');
            $table->integer('municipio_id')->unsigned()->index();
            $table->integer('departamento_id')->unsigned()->index();
            $table->string('email')->nullable();

            $table->integer('tipoapartamento_id')->unsigned()->index();
            $table->integer('tamanio_apartamento');
            $table->integer('tamanio_casa');
            $table->integer('cluster');
            $table->string('bloque');
            $table->integer('edificio_id')->unsigned()->index();
            $table->integer('nivel_piso');
            $table->integer('apartamento');
            $table->integer('lote_casa');
            $table->string('nomenclatura');
            $table->string('nomenclatura_casa');
            $table->double('tamanio_lote_metros');
            $table->double('tamanio_lote_varas');
            $table->integer('capacidad_apartamentos_edificio');
            $table->double('tamanio_edificio');
            $table->integer('apartaments_nivel');
            $table->double('area_comun');
            $table->float('precio_venta');
            $table->float('prima');
            $table->float('monto_financiar');

            $table->integer('user_id')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipoapartamentos');
        Schema::drop('edificios');
        Schema::drop('mancomunados');
        Schema::drop('solicituds');

    }
}


























