<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Mancomunado::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'solicitud_id' => 1,
        'apellido' => $faker->name,
        'estado_civil' => 'casado',
        'profesion' => $faker->name,
        'telefono' => $faker->numberBetween(1111111111, 99999999999),
        'celular' => $faker->numberBetween(1111111111, 99999999999),
        'domicilio' => $faker->text(70),
        'municipio_id' => 1,
        'departamento_id' => 1,
        'email' => $faker->safeEmail,
    ];
});

$factory->define(App\Tipoapartamento::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
    ];
});

$factory->define(App\Solicitud::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'apellido' => $faker->name,
        'estado_civil' => 'casado',
        'profesion' => $faker->name,
        'telefono' => $faker->numberBetween(1111111111, 99999999999),
        'celular' => $faker->numberBetween(1111111111, 99999999999),
        'domicilio' => $faker->text(70),
        'municipio_id' => 1,
        'departamento_id' => 1,
        'email' => $faker->safeEmail,
        'tipoapartamento_id' => 2,
        'tamanio_apartamento' => 43,
        'tamanio_casa' => 55,
        'cluster' => 2,
        'bloque' => "A",
        'edificio_id' => 2,
        'nivel_piso' => 3,
        'apartamento' => 22,
        'lote_casa' => 28,
        'nomenclatura' => "1E-210",
        'nomenclatura_casa' => "1E-28",
        'tamanio_lote_metros' => 112.5,
        'tamanio_lote_varas' => 161.35,
        'capacidad_apartamentos_edificio' => 12,
        'tamanio_edificio' => 552.99,
        'apartaments_nivel' => 8,
        'area_comun' => 40.12,
        'precio_venta' => 400000.00,
        'prima' => 52000.00,
        'monto_financiar' => 348000.00,

    ];
});

$factory->define(App\Solicitud::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'apellido' => $faker->name,
        'estado_civil' => 'casado',
        'profesion' => $faker->name,
        'telefono' => $faker->numberBetween(1111111111, 99999999999),
        'celular' => $faker->numberBetween(1111111111, 99999999999),
        'identidad' => $faker->numberBetween(111111111111, 9999999999999),
        'domicilio' => $faker->text(70),
        'municipio_id' => 1,
        'departamento_id' => 1,
        'email' => $faker->safeEmail,
        'tipoapartamento_id' => 2,
        'tamanio_apartamento' => 43,
        'tamanio_casa' => 55,
        'cluster' => 2,
        'bloque' => "A",
        'edificio_id' => 2,
        'nivel_piso' => 3,
        'apartamento' => 22,
        'lote_casa' => 28,
        'nomenclatura' => "1E-210",
        'nomenclatura_casa' => "1E-28",
        'tamanio_lote_metros' => 112.5,
        'tamanio_lote_varas' => 161.35,
        'capacidad_apartamentos_edificio' => 12,
        'tamanio_edificio' => 552.99,
        'apartaments_nivel' => 8,
        'area_comun' => 40.12,
        'precio_venta' => 400000.00,
        'prima' => 52000.00,
        'monto_financiar' => 348000.00,

    ];
});

$factory->define(App\Ficha::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'identidad' => $faker->numberBetween(111111111111, 9999999999999),
        'email' => $faker->safeEmail,
        'telefono' => $faker->numberBetween(1111111111, 99999999999),
        'celular' => $faker->numberBetween(1111111111, 99999999999),
        'con_nombre' => $faker->name,
        'con_identidad' => $faker->numberBetween(111111111111, 9999999999999),
        'bono' => true,
        'total_ingresos' => 20000.00,
        'fondo_id' => 1,
        'tasa_apox' => 25.5,
        'tipoapartamento_id' => 2,
        'tamanio_apartamento' => 43,
        'tamanio_casa' => 55,
        'cluster' => 2,
        'bloque' => "A",
        'edificio_id' => 2,
        'nivel_piso' => 3,
        'apartamento' => 22,
        'lote_casa' => 28,
        'tamanio_lote_metros' => 112.5,
        'tamanio_lote_varas' => 161.35,
    ];
});
